import React, {useEffect, useState} from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-community/async-storage';

//temporting screens
//import LoginScreen from './Screens/login';
import HomeScreen from './Screens/home';
import LoadScreen from './Screens/loading';
import LoginScreen from './Screens/login';

const Stack = createStackNavigator(); 
const App = () => {

  const [foundToken, setFoundToken] = useState('');
  const [isLoad, setIsLoad] = useState(true);

  
  const checkToken = async () => {
  
    try {
      let findingToken = await AsyncStorage.getItem('token');
      setFoundToken(findingToken);
      setIsLoad(false);
    } catch (error) {
      console.log(error);
    }
  }

  const loginAction = async () => {
    
    let dummyToken = 'CodeSeemToken'

    try{
      await AsyncStorage.setItem('token', dummyToken);
      setFoundToken(dummyToken);
      console.log(dummyToken);
    }catch (error){
      console.log(error);
    }
  }

  const logoutAction = async () => {
    try{
      await AsyncStorage.removeItem('token');
      setFoundToken('');
    }catch (error){
      console.log(error);
    }
  }

  useEffect (() => {
    checkToken();
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {
          foundToken
          ? <Stack.Screen name = "Home">
            {props => <HomeScreen {...props} logout={logoutAction} />}
            </Stack.Screen>
            :(isLoad
              ?<Stack.Screen
                  name="Load"
                  option={{headerShown:false}}>
                  {props => <LoadScreen {...props}/>}
              </Stack.Screen>
              :<Stack.Screen name="Login">
                {props => <LoginScreen {...props} login={loginAction} />}
              </Stack.Screen>
            )
        }
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;












































// You can import from local files
// import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
// import { Card } from 'react-native-paper';

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text style={styles.paragraph}>
//         Change code in the editor and watch it change on your phone! Save to get a shareable url.
//       </Text>
//       <Card>
//         <AssetExample />
//       </Card>
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     paddingTop: Constants.statusBarHeight,
//     backgroundColor: '#ecf0f1',
//     padding: 8,
//   },
//   paragraph: {
//     margin: 24,
//     fontSize: 18,
//     fontWeight: 'bold',
//     textAlign: 'center',
//   },
// });
































// import { StatusBar } from 'expo-status-bar';
// import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';


// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Open up App.js to start working on your app!</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
