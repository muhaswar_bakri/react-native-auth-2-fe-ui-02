import React from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native';

const Login = ({login}) => {
  return (
    <View style={{
      flex:1,
      justifyContent: 'center',
      alignItems:'center'
    }}>
    <Text > you haven logged in yet </Text>
    <br/>
    <Text> Username : </Text>
    <TextInput 
      style={styles.input} 
      placeholder="Input Nama"></TextInput> 
    
    <Text> Password : </Text>
    <TextInput 
      style={styles.input} 
      placeholder="Input Pass"></TextInput> 
    <Button title="Login" onPress={login}/>
    </View>
  );
}

const styles = StyleSheet.create({
  input : {
    borderRadius:5,
    backgroundColor: "white",
    padding: 8,
    margin:15,
    width: 250
  }
})
export default Login;